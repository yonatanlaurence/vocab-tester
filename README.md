# Vocab Tester
This is a pet app written in Svelte.js and Ruby on Rails (API).
The DB is sqlite so no DB setup needed.

### Setup
I use direnv so just cd into the directory and run `direnv allow` to setup the ruby version (2.7.2)
Run `bundle install` and `cd svelte-app && npm install` to install packages for front and back ends.

There is test data to seed. Run `rails db:setup`

### Development
Run the backend with `rails s` and the front-end with `npm run dev` in the svelte-app dir.

Backend is available on localhost:3000 and front-end at localhost:8080

### Deployment
We'll figure that out later.